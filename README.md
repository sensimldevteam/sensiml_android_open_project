# SensiML OpenApp Project
The SensiML OpenApp Project is an Android application that will show Bluetooth-LE classifications from devices flashed with a BLE enabled SensiML Knowledge Pack

## SensiML Classifier BLE Characteristic UUIDs
~~~
SENSIML_EVENT_SERVICE_UUID = 534D1100-9B35-4933-9B10-52FFA9740042
SENSIML_EVENT_CHARACTERISTIC_UUID = 534D1101-9B35-4933-9B10-52FFA9740042
~~~

### Note:
If you are using a QuickAI device, the UUIDs are different from the standard SensiML Classifier BLE UUIDs. Open the file SensiMLClassifier.java and uncomment the QuickAI UUIDs to use the OpenApp Project with a QuickAI device.

~~~
SENSIML_EVENT_SERVICE_UUID = 42421100-5A22-46DD-90F7-7AF26F723159
SENSIML_EVENT_CHARACTERISTIC_UUID = 42421101-5A22-46DD-90F7-7AF26F723159
~~~

## SensiML Classifier onCharacteristicChanged Event
A SensiML classification will return a Model_ID and Class_ID from the BLE onCharacteristicChanged event. These values correspond to your Knowledge Pack class map and model.

The class map can be found inside the Analytics Studio 'Create Knowledge Pack' widget or by running the following commands in a new cell through the Analytics Studio:
~~~
kp = dsk.get_knowledgepack("ENTER-UUID")
kp.class_map
~~~

See the reference code below for how to read the live classification ModelID and ClassID from the onCharacteristicChanged BLE event in Android. This code is found inside the activity DeviceStatusBaseActivity.java
~~~
final int MODEL_NUM_INDEX = 0;
final int CLASSIFICATION_INDEX = 2;
byte[] data = intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA_EVENT_CLASSIFICATION);
ByteBuffer wrapBuffer = ByteBuffer.wrap(data);
wrapBuffer.order(ByteOrder.LITTLE_ENDIAN);
int modelId = (int) wrapBuffer.getChar(MODEL_NUM_INDEX);
int classId = (int) wrapBuffer.getChar(CLASSIFICATION_INDEX);
~~~

## Notes:
* Android version 4.4 is required

