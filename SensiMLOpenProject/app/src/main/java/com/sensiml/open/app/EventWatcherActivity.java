package com.sensiml.open.app;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.sensiml.open.common.ble.subscribe.BluetoothLeService;
import com.sensiml.open.common.ble.subscribe.ServiceWatcherActivity;
import com.sensiml.open.common.object.Event;
import com.sensiml.open.common.utilities.UncaughtExceptionHandler;

import java.util.ArrayList;

public class EventWatcherActivity extends ServiceWatcherActivity {

    @NonNull
    private ArrayList<Event> mEventHistory = new ArrayList<>();

    @Nullable
    private Handler mHandler;

    private int CLASS_FONT_SIZE = 90;
    private int MODEL_FONT_SIZE = 20;

    TextSwitcher mSwitcherLastModel;
    TextSwitcher mSwitcherLastClass;
    LinearLayout mLayoutListEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(this));

        LayoutInflater inflater = LayoutInflater.from(this);
        View inflatedLayout= inflater.inflate(R.layout.layout_event_watcher, new LinearLayout(this), false);
        LinearLayout layoutDeviceConnected = findViewById(R.id.fragment_service_watcher_layout_device_connected);
        layoutDeviceConnected.addView(inflatedLayout);

        mLayoutListEvents = findViewById(R.id.fragment_service_watcher_list_events);

        mSwitcherLastClass = findViewById(R.id.fragment_service_watcher_text_last_event_class);
        mSwitcherLastClass.setInAnimation(this, R.anim.slide_in_right);
        mSwitcherLastClass.setOutAnimation(this, R.anim.slide_out_left);
        mSwitcherLastClass.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                TextView textLastEvent = new TextView(EventWatcherActivity.this);
                textLastEvent.setPadding(0,0,0,0);
                textLastEvent.setIncludeFontPadding(false);
                textLastEvent.setTextSize(TypedValue.COMPLEX_UNIT_SP, CLASS_FONT_SIZE);
                textLastEvent.setTextColor(getResources().getColor(R.color.color_text));
                textLastEvent.setGravity(Gravity.CENTER_HORIZONTAL);
                return textLastEvent;
            }
        });

        mSwitcherLastModel = findViewById(R.id.fragment_service_watcher_text_last_event_model);
        mSwitcherLastModel.setInAnimation(this, R.anim.slide_in_right);
        mSwitcherLastModel.setOutAnimation(this, R.anim.slide_out_left);
        mSwitcherLastModel.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                TextView textLastEvent = new TextView(EventWatcherActivity.this);
                textLastEvent.setIncludeFontPadding(false);
                textLastEvent.setPadding(0,0,0,0);
                textLastEvent.setTextSize(TypedValue.COMPLEX_UNIT_SP, MODEL_FONT_SIZE);
                textLastEvent.setTextColor(getResources().getColor(R.color.color_text));
                textLastEvent.setGravity(Gravity.CENTER_HORIZONTAL);
                return textLastEvent;
            }
        });

        if(savedInstanceState != null){
            if(savedInstanceState.containsKey("EventHistory")){
                ArrayList<Event> eventHistory = savedInstanceState.getParcelableArrayList("EventHistory");
                if(eventHistory != null){
                    mEventHistory = eventHistory;
                    mSwitcherLastClass.setText("Idle");
                }
            }
        }

        refreshListViewEventAdapter();
    }

    private void findClassId(@NonNull Event event){
        int modelId = event.getModelId();
        int classId = event.getClassId();

        String modelName = "Model " + String.valueOf(modelId);
        String className = String.valueOf(classId);

        event.setModelName(modelName);
        event.setClassName(className);

        mSwitcherLastModel.setText(modelName);
        mSwitcherLastClass.setText(className);
    }

    @Override
    public void addEventToHistory(@NonNull Event event){
        findClassId(event);
        mEventHistory.add(event);
        addLayoutEvent(event);

        //Post idle status if no classifications occurred in 6 seconds
        if(mHandler != null){
            mHandler.removeCallbacksAndMessages(null);
        }else{
            mHandler = new Handler();
        }
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwitcherLastClass.setText("Idle");
                mSwitcherLastModel.setText("");
            }
        }, 6000);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putParcelableArrayList("EventHistory", mEventHistory);
        super.onSaveInstanceState(outState);
    }

    private void addLayoutEvent(@NonNull Event event){
        LayoutInflater inflater = LayoutInflater.from(this);
        View childView = inflater.inflate(R.layout.list_item_event, new LinearLayout(this), false);

        TextView textName = childView.findViewById(R.id.list_item_event_class_name);
        textName.setText(event.getClassName());

        TextView textModelName = childView.findViewById(R.id.list_item_event_model_name);

        if(event.getModelName() != null) {
            textModelName.setText(event.getModelName());
        } else{
            textModelName.setVisibility(View.GONE);
        }

        TextView textTime = childView.findViewById(R.id.list_item_event_time);
        textTime.setText(event.getDate());

        if(mLayoutListEvents.getChildCount() != 0){
            mLayoutListEvents.addView(inflater.inflate(R.layout.separator_light, new LinearLayout(this), false), 0);
        }
        mLayoutListEvents.addView(childView, 0);
    }

    private void refreshListViewEventAdapter(){
        if(mLayoutListEvents.getChildCount() > 0){
            mLayoutListEvents.removeAllViews();
        }

        for(int i = 0; i < mEventHistory.size(); i++){
            addLayoutEvent(mEventHistory.get(i));
        }
    }

    private void clearHistory(){
        mSwitcherLastClass.setText("");
        mSwitcherLastModel.setText("");
        mEventHistory = new ArrayList<>();
        refreshListViewEventAdapter();
    }

    @Override
    public void removeDevice(){
        clearHistory();
        super.removeDevice();
    }

    @Override
    public boolean hasRequiredServices(@NonNull BluetoothLeService bluetoothLeService){
        return bluetoothLeService.hasSensiMLCharacteristic();
    }

    @Override
    public void startBleNotifications(@NonNull BluetoothLeService bluetoothLeService){
        bluetoothLeService.startSensiMLCharacteristicNotification();
    }
}
