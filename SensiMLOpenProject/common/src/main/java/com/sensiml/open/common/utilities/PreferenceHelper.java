package com.sensiml.open.common.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sensiml.open.common.Preference;

public class PreferenceHelper {

    @NonNull
    private static SharedPreferences.Editor getPreferenceEditor(@NonNull Context context){
        SharedPreferences preferences = context.getSharedPreferences(Preference.APP, Context.MODE_PRIVATE);
        return preferences.edit();
    }

    public static void savePreference(@NonNull Context context, @NonNull String preference, @Nullable String valueToSave) {
        SharedPreferences.Editor prefsEditor = getPreferenceEditor(context);
        prefsEditor.putString(preference, valueToSave);
        prefsEditor.apply();
    }
    
    public static void savePreference(@NonNull Context context, @NonNull String preference, boolean valueToSave){
        SharedPreferences.Editor prefsEditor = getPreferenceEditor(context);
        prefsEditor.putBoolean(preference, valueToSave);
        prefsEditor.apply();
    }

    public static void savePreference(@NonNull Context context, @NonNull String preference, int valueToSave){
        SharedPreferences.Editor prefsEditor = getPreferenceEditor(context);
        prefsEditor.putInt(preference, valueToSave);
        prefsEditor.apply();
    }

    @Nullable
    public static String getPreferenceString(@NonNull Context context, String preferenceName) {
        SharedPreferences preferences = context.getSharedPreferences(Preference.APP, Context.MODE_PRIVATE);
        return preferences.getString(preferenceName, null);
    }

    public static boolean getPreferenceBoolean(@NonNull Context context, String preferenceName) {
        SharedPreferences preferences = context.getSharedPreferences(Preference.APP, Context.MODE_PRIVATE);
        return preferences.getBoolean(preferenceName, false);
    }

    public static boolean getPreferenceBoolean(@NonNull Context context, String preferenceName, boolean defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(Preference.APP, Context.MODE_PRIVATE);
        return preferences.getBoolean(preferenceName, defaultValue);
    }

    public static int getPreferenceInt(@NonNull Context context, String preferenceName) {
        SharedPreferences preferences = context.getSharedPreferences(Preference.APP, Context.MODE_PRIVATE);
        return preferences.getInt(preferenceName, 0);
    }
    public static int getPreferenceInt(@NonNull Context context, String preferenceName, int defaultValue) {
        SharedPreferences preferences = context.getSharedPreferences(Preference.APP, Context.MODE_PRIVATE);
        return preferences.getInt(preferenceName, defaultValue);
    }
}
