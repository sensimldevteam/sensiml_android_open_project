package com.sensiml.open.common.ble.devices;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;

import com.sensiml.open.common.ble.subscribe.BluetoothLeService;

import java.util.UUID;

import static com.sensiml.open.common.ble.subscribe.BluetoothLeService.EXTRA_DATA_EVENT_CLASSIFICATION;

public class SensiMLClassifier extends SensiMLSupportedDevice {

    public final static UUID SENSIML_EVENT_SERVICE_UUID = UUID.fromString("534D1100-9B35-4933-9B10-52FFA9740042");
    private final static UUID SENSIML_EVENT_CHARACTERISTIC_UUID = UUID.fromString("534D1101-9B35-4933-9B10-52FFA9740042");

    //QUICKAI UUIDs
    //public final static UUID SENSIML_EVENT_SERVICE_UUID = UUID.fromString("42421100-5A22-46DD-90F7-7AF26F723159");
    //private final static UUID SENSIML_EVENT_CHARACTERISTIC_UUID = UUID.fromString("42421101-5A22-46DD-90F7-7AF26F723159");

    public SensiMLClassifier(BluetoothGatt gatt) {
        final BluetoothGattService mClassifierService = gatt.getService(SENSIML_EVENT_SERVICE_UUID);
        if (mClassifierService != null) {
            mEventClassificationCharacteristic = mClassifierService.getCharacteristic(SENSIML_EVENT_CHARACTERISTIC_UUID);
        }
    }

    @Override
    public Intent processIncomingData(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        if (characteristic.equals(mEventClassificationCharacteristic)) {
            final Intent intent = new Intent(BluetoothLeService.ACTION_SENSIML_EVENT_NOTIFICATION);
            intent.putExtra(EXTRA_DATA_EVENT_CLASSIFICATION, characteristic.getValue());
            return intent;
        }
        return null;
    }

    @Override
    public boolean hasSensiMLClassifierCharacteristics(){
        return (mEventClassificationCharacteristic != null);
    }
}
