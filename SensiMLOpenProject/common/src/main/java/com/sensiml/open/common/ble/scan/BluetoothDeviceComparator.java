package com.sensiml.open.common.ble.scan;

import android.bluetooth.BluetoothDevice;

import java.util.Comparator;

//Allows us to sort a bluetooth device list by name, and puts all of the unnamed devices at the bottom
public class BluetoothDeviceComparator implements Comparator<BluetoothDevice> {
    @Override
    public int compare(BluetoothDevice bluetoothDevice1, BluetoothDevice bluetoothDevice2) {
        String name1 = bluetoothDevice1.getName();
        if(name1 == null){
            name1 = "zzz";
        }
        String name2 = bluetoothDevice2.getName();
        if(name2 == null){
            name2 = "zzz";
        }

        //If the names are the same sort by address
        if(name1.compareTo(name2) == 0){
            return bluetoothDevice1.getAddress().compareTo(bluetoothDevice2.getAddress());
        }
        return name1.compareTo(name2);
    }
}
