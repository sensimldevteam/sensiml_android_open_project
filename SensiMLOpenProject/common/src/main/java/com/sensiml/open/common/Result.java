package com.sensiml.open.common;

public final class Result {
    public final static String BLE_DEVICE_NAME = "BleDeviceNameResult";
    public final static String BLE_DEVICE_ADDRESS = "BleDeviceAddressResult";
}
