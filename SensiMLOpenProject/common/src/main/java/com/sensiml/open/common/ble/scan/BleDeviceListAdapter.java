package com.sensiml.open.common.ble.scan;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.sensiml.open.common.R;

import java.util.List;

public class BleDeviceListAdapter extends BaseAdapter {

    @Nullable
    private Context mContext;

    @NonNull
    private List<BluetoothDevice> mData;

    private static class ViewHolder {
        TextView textDeviceAddress;
        TextView textDeviceName;
    }

    BleDeviceListAdapter(@NonNull Context context, @NonNull List<BluetoothDevice> data) {
        mData = data;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public BluetoothDevice getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, @Nullable View convertView, ViewGroup parent) {
        if (mContext == null) return null;

        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item_ble_device, parent, false);
            holder = new ViewHolder();
            holder.textDeviceAddress = convertView.findViewById(R.id.list_item_ble_device_text_device_address);
            holder.textDeviceName = convertView.findViewById(R.id.list_item_ble_device_text_device_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        BluetoothDevice item = getItem(position);
        holder.textDeviceAddress.setText(item.getAddress());
        if(item.getName() == null){
            holder.textDeviceName.setText(mContext.getResources().getString(R.string.unnamed_device));
        }else{
            holder.textDeviceName.setText(item.getName());
        }

        return convertView;
    }
}
