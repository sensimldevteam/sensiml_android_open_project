package com.sensiml.open.common.object;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Event implements Parcelable {

    private int modelId;

    private int classId;

    @Nullable
    private String modelName;

    @Nullable
    private String className;

    @NonNull
    private String date;

    public Event(int modelId, int classId, @NonNull String date){
        this.modelId = modelId;
        this.classId = classId;
        this.date = date;
    }

    protected Event(Parcel in) {
        modelId = in.readInt();
        classId = in.readInt();
        modelName = in.readString();
        className = in.readString();
        date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(modelId);
        parcel.writeInt(classId);
        parcel.writeString(modelName);
        parcel.writeString(className);
        parcel.writeString(date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    @Nullable
    public String getModelName() {
        return modelName;
    }

    public void setModelName(@Nullable String modelName) {
        this.modelName = modelName;
    }

    @Nullable
    public String getClassName() {
        return className;
    }

    public void setClassName(@Nullable String className) {
        this.className = className;
    }
}
