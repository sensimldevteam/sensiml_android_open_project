package com.sensiml.open.common.ble.scan;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.location.LocationManagerCompat;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sensiml.open.common.R;
import com.sensiml.open.common.Result;
import com.sensiml.open.common.utilities.PermissionHelper;
import com.sensiml.open.common.utilities.UncaughtExceptionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class DeviceScanActivity extends AppCompatActivity {

    @Nullable
    private BluetoothAdapter mBluetoothAdapter;

    @Nullable
    private BluetoothLeScanner mBluetoothLeScanner;

    @Nullable
    BleDeviceListAdapter mDeviceListAdapter;

    private boolean mScanning;
    private HashMap<String, BluetoothDevice> mScanResults;
    private ScanCallback mScanCallback;

    @Nullable
    private Handler mHandler;

    @NonNull
    private List<BluetoothDevice> mBlueToothDevices = new ArrayList<>();

    private Button mButtonScan;

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            addDevice(device);
                        }
                    });
                }
            };

    private static final int REQUEST_ENABLE_BT = 100;
    private static final int REQUEST_FINE_LOCATION = 200;

    private static final long SCAN_PERIOD = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_scan);

        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(this));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView textToolbarTitle = findViewById(R.id.toolbar_title);
        textToolbarTitle.setText(getResources().getString(R.string.title_activity_device_scan));

        ImageButton buttonBack = findViewById(R.id.toolbar_image_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        if(bluetoothManager != null){
            mBluetoothAdapter = bluetoothManager.getAdapter();
        }

        setupDeviceList();

        mButtonScan = findViewById(R.id.fragment_device_scan_button_scan);
        mButtonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mScanning){
                    stopScanDevices(true);
                }
                else {
                    scanForDevices();
                }
            }
        });
        scanForDevices();
    }

    @Override
    public void onDestroy() {
        if(mScanning){
            stopScanDevices(true);
        }
        super.onDestroy();
    }

    private void showScanningViews() {
        LinearLayout layoutScanning = findViewById(R.id.fragment_device_scan_layout_scanning);
        if(mScanning){
            layoutScanning.setVisibility(View.VISIBLE);
            mButtonScan.setText(getResources().getString(R.string.action_stop));
        }else{
            layoutScanning.setVisibility(View.GONE);
            mButtonScan.setText(getResources().getString(R.string.action_scan));
        }
    }

    private void scanForDevices(){
        if (!hasPermissions()) return;

        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if(locationManager != null && !LocationManagerCompat.isLocationEnabled(locationManager)){
            Toast.makeText(this, "Must have Location enabled in Android Settings to scan for devices", Toast.LENGTH_LONG).show();
        }

        mScanResults = new HashMap<>();
        mScanning = true;
        showScanningViews();

        mHandler = new Handler();
        // Stops scanning after a pre-defined scan period.
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScanDevices(true);
            }
        }, SCAN_PERIOD);

        if (Build.VERSION.SDK_INT >= 21) {
            startScan();
        } else{
            scanLeDevice();
        }
    }

    private void stopScanDevices(boolean sort){
        mScanning = false;
        showScanningViews();

        if(sort){
            Collections.sort(mBlueToothDevices, new BluetoothDeviceComparator());
            if (mDeviceListAdapter != null) {
                mDeviceListAdapter.notifyDataSetChanged();
            }
        }

        if(mHandler != null){
            mHandler.removeCallbacksAndMessages(null);
        }
        mHandler = null;

        if (Build.VERSION.SDK_INT >= 21) {
            stopScan();
        } else{
            stopLeScan();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startScan() {
        List<ScanFilter> filters = new ArrayList<>();
        ScanFilter.Builder knowledgePackFilter = new ScanFilter.Builder();
        filters.add(knowledgePackFilter.build());

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build();

        mScanResults = new HashMap<>();
        mScanCallback = new BleScanCallback(mScanResults);

        if(mBluetoothAdapter != null){
            mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
            mBluetoothLeScanner.startScan(filters, settings, mScanCallback);
        }else{
            stopScanDevices(true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void stopScan() {
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled() && mBluetoothLeScanner != null) {
            mBluetoothLeScanner.stopScan(mScanCallback);
        }
        mScanCallback = null;
    }

    private void scanLeDevice() {
        if (mBluetoothAdapter == null) return;

        mBluetoothAdapter.startLeScan(mLeScanCallback);
    }

    private void stopLeScan(){
        if(mBluetoothAdapter == null) return;

        mBluetoothAdapter.stopLeScan(mLeScanCallback);
    }

    private void addDevice(BluetoothDevice bluetoothDevice){
        if(!mBlueToothDevices.contains(bluetoothDevice)) {
            mBlueToothDevices.add(bluetoothDevice);

            if (mDeviceListAdapter != null) {
                mDeviceListAdapter.notifyDataSetChanged();
            }
        }
    }

    private void setupDeviceList(){
        ListView listViewProjects = findViewById(R.id.fragment_device_scan_list_bluetooth_devices);
        mDeviceListAdapter = new BleDeviceListAdapter(getApplicationContext(), mBlueToothDevices);
        listViewProjects.setAdapter(mDeviceListAdapter);
        listViewProjects.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                BluetoothDevice selectedDevice = mBlueToothDevices.get(i);

                if(mScanning){
                    stopScanDevices(false);
                }

                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                returnIntent.putExtra(Result.BLE_DEVICE_NAME, selectedDevice.getName());
                returnIntent.putExtra(Result.BLE_DEVICE_ADDRESS, selectedDevice.getAddress());
                finish();
            }
        });
    }

    private boolean hasPermissions() {
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            requestBluetoothEnable();
            return false;
        } else if (!PermissionHelper.askPermission(this, REQUEST_FINE_LOCATION, new String[] { Manifest.permission.ACCESS_FINE_LOCATION })) {
            return false;
        }
        return true;
    }

    private void requestBluetoothEnable() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Note: If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0) {
            switch (requestCode) {
                case REQUEST_FINE_LOCATION: {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        scanForDevices();
                    }else{
                        stopScanDevices(false);
                    }
                }
            }
        } else {
            Toast.makeText(getApplicationContext(), "Permission cancelled. Cannot load Ble Devices", Toast.LENGTH_LONG).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private class BleScanCallback extends ScanCallback {
        BleScanCallback(HashMap<String, BluetoothDevice> scanResults) {
            mScanResults = scanResults;
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            addScanResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                addScanResult(result);
            }
        }
        @Override
        public void onScanFailed(int errorCode) {
            Toast.makeText(getApplicationContext(), "BLE Scan Failed with code " + errorCode, Toast.LENGTH_LONG).show();
        }

        private void addScanResult(ScanResult result) {
            BluetoothDevice device = result.getDevice();
            String deviceAddress = device.getAddress();
            mScanResults.put(deviceAddress, device);

            addDevice(device);
        }
    }
}
