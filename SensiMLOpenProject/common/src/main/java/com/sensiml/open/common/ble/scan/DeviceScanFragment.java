package com.sensiml.open.common.ble.scan;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sensiml.open.common.R;


/**
 * A placeholder fragment containing a simple view.
 */
public class DeviceScanFragment extends Fragment {

    public DeviceScanFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_scan, container, false);
    }
}
