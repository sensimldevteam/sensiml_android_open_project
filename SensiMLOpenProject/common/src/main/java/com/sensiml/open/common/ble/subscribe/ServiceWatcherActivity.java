package com.sensiml.open.common.ble.subscribe;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sensiml.open.common.R;
import com.sensiml.open.common.Preference;
import com.sensiml.open.common.Result;
import com.sensiml.open.common.ble.scan.DeviceScanActivity;
import com.sensiml.open.common.utilities.PreferenceHelper;
import com.sensiml.open.common.utilities.UncaughtExceptionHandler;

public class ServiceWatcherActivity extends DeviceStatusBaseActivity {

    public static final int SETUP_DEVICE_REQUEST = 1;

    private Button mButtonConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_watcher);

        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(this));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView textToolbarTitle = findViewById(R.id.toolbar_title);
        textToolbarTitle.setText(getResources().getString(R.string.app_title));

        ImageButton buttonBack = findViewById(R.id.toolbar_image_back);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        setupBleDeviceControls();

        Button buttonDeviceRemove = findViewById(R.id.fragment_service_watcher_button_device_remove);
        buttonDeviceRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeDevice();
                showDeviceConnectionUI(false);
            }
        });

        mButtonConnect = findViewById(R.id.fragment_service_watcher_button_device_connect);
        mButtonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isDeviceConnected()){
                    BluetoothLeService bluetoothLeService = getBluetoothLeService();
                    if (bluetoothLeService != null) {
                        bluetoothLeService.disconnect();
                    }
                }else{
                    connectToDevice();
                }
            }
        });
    }

    @Override
    public void updateConnectionStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textDeviceStatus = findViewById(R.id.fragment_service_watcher_text_device_status);
                textDeviceStatus.setText(status);

                if(isDeviceConnected()){
                    mButtonConnect.setText(getResources().getString(R.string.disconnect));
                }else{
                    mButtonConnect.setText(getResources().getString(R.string.connect));
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_service_watcher, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(itemId == R.id.menu_service_watcher_find_device) {
            BluetoothLeService bluetoothLeService = getBluetoothLeService();
            if (bluetoothLeService != null) {
                bluetoothLeService.disconnect();
            }
            Intent intent = new Intent(getApplicationContext(), DeviceScanActivity.class);
            startActivityForResult(intent, SETUP_DEVICE_REQUEST);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                switch (requestCode) {
                    case SETUP_DEVICE_REQUEST:

                        removeDevice();

                        setBleDeviceName(data.getStringExtra(Result.BLE_DEVICE_NAME));
                        setBleDeviceAddress(data.getStringExtra(Result.BLE_DEVICE_ADDRESS));

                        PreferenceHelper.savePreference(getApplicationContext(), Preference.BLE_DEVICE_NAME_PREFERENCE, getBleDeviceName());
                        PreferenceHelper.savePreference(getApplicationContext(), Preference.BLE_DEVICE_ADDRESS_PREFERENCE, getBleDeviceAddress());

                        setupBleDeviceControls();
                }
        }
    }

    public void setupBleDeviceControls(){
        String bleDeviceAddress = getBleDeviceAddress();
        String bleDeviceName = getBleDeviceName();
        if(bleDeviceAddress != null){
            TextView textDeviceAddress = findViewById(R.id.fragment_service_watcher_text_device_address);
            textDeviceAddress.setText(bleDeviceAddress);

            if(bleDeviceName == null){
                bleDeviceName = getResources().getString(R.string.unnamed_device);
            }
            TextView textDeviceName = findViewById(R.id.fragment_service_watcher_text_device_name);
            textDeviceName.setText(bleDeviceName);

            showDeviceConnectionUI(true);
        } else{
            showDeviceConnectionUI(false);
        }
    }

    public void removeDevice(){
        BluetoothLeService bluetoothLeService = getBluetoothLeService();
        if (bluetoothLeService != null) {
            bluetoothLeService.disconnect();
        }

        setBleDeviceAddress(null);
        setBleDeviceName(null);

        PreferenceHelper.savePreference(getApplicationContext(), Preference.BLE_DEVICE_NAME_PREFERENCE, null);
        PreferenceHelper.savePreference(getApplicationContext(), Preference.BLE_DEVICE_ADDRESS_PREFERENCE, null);
    }

    private void showDeviceConnectionUI(boolean hasDevice){
        LinearLayout layoutFindDevices = findViewById(R.id.fragment_service_watcher_layout_device_not_connected);
        LinearLayout layoutDeviceInformation = findViewById(R.id.fragment_service_watcher_layout_device_connected);

        if(hasDevice){
            layoutFindDevices.setVisibility(View.GONE);
            layoutDeviceInformation.setVisibility(View.VISIBLE);
        }else{
            layoutFindDevices.setVisibility(View.VISIBLE);
            layoutDeviceInformation.setVisibility(View.GONE);
        }
    }
}
