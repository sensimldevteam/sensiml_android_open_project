package com.sensiml.open.common.utilities;

import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

public class PermissionHelper {
    public static boolean askPermission(@NonNull Activity activity, int requestId, @NonNull String[] permissionNames) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {

            if (!hasPermissions(activity, permissionNames)) {
                activity.requestPermissions(permissionNames, requestId);
                return false;
            }
        }
        return true;
    }

    public static boolean hasPermissions(@NonNull Activity activity, @NonNull String[] permissionNames){
        boolean hasPermissions = true;
        for(String permissionName :  permissionNames){

            int permission = ActivityCompat.checkSelfPermission(activity, permissionName);
            if (permission != PackageManager.PERMISSION_GRANTED) {
                hasPermissions = false;
            }
        }
        return  hasPermissions;
    }
}
