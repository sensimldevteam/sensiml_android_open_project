package com.sensiml.open.common.ble.subscribe;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sensiml.open.common.R;

public class ServiceWatcherFragment extends Fragment {

    public ServiceWatcherFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_watcher, container, false);
    }
}
