package com.sensiml.open.common;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class UnexpectedErrorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unexpected_error);

        final String error = getIntent().getStringExtra("error");

        Button emailButton = findViewById(R.id.activity_critical_error_button_send_email);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");
                String to[] = {"username@youremail.com"};
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                emailIntent.putExtra(Intent.EXTRA_TEXT, error);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Unexpected error log");
                startActivity(Intent.createChooser(emailIntent , "Send email..."));
            }
        });
    }
}