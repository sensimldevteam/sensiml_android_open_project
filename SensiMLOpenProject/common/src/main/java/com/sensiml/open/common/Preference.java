package com.sensiml.open.common;

public final class Preference {
    public final static String BLE_DEVICE_NAME_PREFERENCE = "BleDeviceNamePreference";
    public final static String BLE_DEVICE_ADDRESS_PREFERENCE = "BleDeviceAddressPreference";
    public final static String APP = "SensiMLApp";
}
