package com.sensiml.open.common.ble.devices;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import androidx.annotation.Nullable;

public class SensiMLSupportedDevice {
    @Nullable
    public BluetoothGattCharacteristic mEventClassificationCharacteristic;

    public @Nullable Intent processIncomingData(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) { return null; }

    public boolean hasSensiMLClassifierCharacteristics() {
        return false;
    }
}
