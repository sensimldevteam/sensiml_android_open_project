package com.sensiml.open.common.ble.subscribe;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.sensiml.open.common.R;
import com.sensiml.open.common.Preference;
import com.sensiml.open.common.object.Event;
import com.sensiml.open.common.utilities.PreferenceHelper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Activity for listening to Device ble services. Extend this activity to update UI based on BLE services
 */
public class DeviceStatusBaseActivity extends AppCompatActivity {

    private boolean mIsConnected;

    private final static String TAG = ServiceWatcherActivity.class.getSimpleName();

    @Nullable
    private String mBleDeviceName;

    @Nullable
    private String mBleDeviceAddress;

    @Nullable
    private BluetoothLeService mBluetoothLeService;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            connectToDevice();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the BluetoothLeService
    public final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_DEVICE_CONNECTED.equals(action)) {
                mIsConnected = true;
                updateConnectionStatus(getResources().getString(R.string.connected));
            } else if (BluetoothLeService.ACTION_DEVICE_DISCONNECTED.equals(action)) {
                mIsConnected = false;
                updateConnectionStatus(getResources().getString(R.string.disconnected));
            } else if (BluetoothLeService.ACTION_SERVICES_DISCOVERED.equals(action)) {
                if (mBluetoothLeService != null){
                    boolean result = hasRequiredServices(mBluetoothLeService);
                    if(!result){
                        updateConnectionStatus(getResources().getString(R.string.missing_service_message));
                    }else{
                        startBleNotifications(mBluetoothLeService);
                    }
                }
            } else if (BluetoothLeService.ACTION_DEVICE_CONNECTION_REFRESHED.equals(action)) {
                mIsConnected = true;
                updateConnectionStatus(getResources().getString(R.string.connected));
                if (mBluetoothLeService != null) {
                    boolean result = hasRequiredServices(mBluetoothLeService);
                    if (!result) {
                        updateConnectionStatus(getResources().getString(R.string.missing_service_message));
                    }else{
                        startBleNotifications(mBluetoothLeService);
                    }
                }
            } else if (BluetoothLeService.ACTION_SENSIML_EVENT_NOTIFICATION.equals(action)) {

                final int MODEL_NUM_INDEX = 0;
                final int CLASSIFICATION_INDEX = 2;
                try {
                    byte[] data = intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA_EVENT_CLASSIFICATION);
                    ByteBuffer wrapBuffer = ByteBuffer.wrap(data);
                    wrapBuffer.order(ByteOrder.LITTLE_ENDIAN);
                    int modelId = (int) wrapBuffer.getChar(MODEL_NUM_INDEX);
                    int classId = (int) wrapBuffer.getChar(CLASSIFICATION_INDEX);

                    SimpleDateFormat s = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss", Locale.getDefault());
                    Event event = new Event(modelId, classId, s.format(new Date()));
                    addEventToHistory(event);
                } catch (Exception e) {
                    Toast.makeText(context, "Could not read event classification from device. Invalid format.", Toast.LENGTH_SHORT).show();
                }
            }
            else if (BluetoothLeService.ACTION_CHARACTERISTIC_FAILED.equals(action)) {
                retryConnection();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBleDeviceAddress = PreferenceHelper.getPreferenceString(getApplicationContext(), Preference.BLE_DEVICE_ADDRESS_PREFERENCE);
        mBleDeviceName = PreferenceHelper.getPreferenceString(getApplicationContext(), Preference.BLE_DEVICE_NAME_PREFERENCE);

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        connectToDevice();
    }

    @Override
    protected void onPause(){
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unbindService(mServiceConnection);
    }

    /**
     * Method that gets called when classification event occurs. Extend ServiceWatcherActivity to override this method
     * This decouples the code for when we don't need the classification characteristic
     */
    public void addEventToHistory(Event event){
        //Leave empty for overrides
    }

    /**
     * Method that gets called after Ble services are discovered. Extend DeviceStatusBaseActivity and override this method
     * This allows multiple activities to share the ServiceWatcher for connecting to a device, but then have different
     * behavior depending on the BleService the activity needs
     */
    public boolean hasRequiredServices(@NonNull BluetoothLeService bluetoothLeService){
        throw new UnsupportedOperationException("BLE service check not implemented");
    }

    /**
     * Method that gets called if the hasRequiredServices returns a connected state. Extend DeviceStatusBaseActivity and override this method
     * This allows multiple activities to share the ServiceWatcher, but have different behavior depending on the services needed
     */
    public void startBleNotifications(@NonNull BluetoothLeService bluetoothLeService){
        throw new UnsupportedOperationException("BLE notifications not implemented");
    }
    /**
     * Method that gets called when the Ble device status changes. Extend DeviceStatusBaseActivity and override this method for implementation
     * @param status The new status
     */
    public void updateConnectionStatus(final String status) {
        throw new UnsupportedOperationException("BLE connection state not implemented");
    }

    public void connectToDevice(){
        if (mBluetoothLeService != null) {
            updateConnectionStatus(getResources().getString(R.string.connecting));
            mBluetoothLeService.connect(mBleDeviceAddress);
        }
    }

    public void retryConnection(){
        updateConnectionStatus(getResources().getString(R.string.characteristic_failed));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mBluetoothLeService != null){
                    connectToDevice();
                }
            }
        }, 3000);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_DEVICE_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_DEVICE_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DEVICE_CONNECTION_REFRESHED);
        intentFilter.addAction(BluetoothLeService.ACTION_CHARACTERISTIC_FAILED);
        intentFilter.addAction(BluetoothLeService.ACTION_SENSIML_EVENT_NOTIFICATION);
        return intentFilter;
    }

    public boolean isDeviceConnected(){
        return mIsConnected;
    }

    @Nullable
    public BluetoothLeService getBluetoothLeService(){
        return mBluetoothLeService;
    }

    @Nullable
    public String getBleDeviceName(){
        return mBleDeviceName;
    }

    public void setBleDeviceName(@Nullable String bleDeviceName){
        mBleDeviceName = bleDeviceName;
    }

    @Nullable
    public String getBleDeviceAddress(){
        return mBleDeviceAddress;
    }

    public void setBleDeviceAddress(@Nullable String bleDeviceAddress){
        mBleDeviceAddress = bleDeviceAddress;
    }
}
